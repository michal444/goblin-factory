﻿using GoblinFactory.Contract.Service;
using System.Collections.Generic;
using GoblinFactory.Contract.DataContracts;
using GoblinFactory.Contract.Repository;

namespace GoblinFactory.Service.Service
{
    public class NewsService : INewsService
    {
        private readonly INewsRepository _newsRepository;

        public NewsService(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        public List<NewsDto> GetAll()
        {
            return _newsRepository.GetAll();
        }
    }
}
