﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;
using GoblinFactory.DataAccess.Models;
using GoblinFactory.Contract.DataContracts;

namespace GoblinFactory.DataAccess.ProjectionExpression
{
    internal class NewsDtoProjectionExpression
    {
        public static Expression<Func<News, NewsDto>> GetAllProjectionExpression()
        {
            Expression<Func<News, NewsDto>> dto = x => new NewsDto()
            {
                Id = x.Id,
                Title = x.Title,
                IsDeleted = x.IsDeleted,
                Description = x.Description,
                DataHistory = x.DataHistory
            };

            return dto;
        }
    }
}
