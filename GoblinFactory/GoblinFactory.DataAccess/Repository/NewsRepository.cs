﻿using GoblinFactory.Contract.Repository;
using System.Collections.Generic;
using GoblinFactory.Contract.DataContracts;
using System.Linq;
using GoblinFactory.DataAccess.ProjectionExpression;

namespace GoblinFactory.DataAccess.Repository
{
    public class NewsRepository : INewsRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public NewsRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<NewsDto> GetAll()
        {
            return _dbContext.News
                .Where(w => w.IsDeleted == false)
                .Select(
                    NewsDtoProjectionExpression.GetAllProjectionExpression()
                ).ToList();
        }
    }
}
