﻿using GoblinFactory.Contract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoblinFactory.DataAccess.Models
{
    public class Topic
    {
        public Topic()
        {
            DataHistory = new DataHistory();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string MyProperty { get; set; }

        public string Description { get; set; }
        public bool IsDeleted { get; set; }

        public int ForumSectionId { get; set; }
        public virtual ForumSubSection ForumSection { get; set; }

        public DataHistory DataHistory { get; set; }
    }
}
