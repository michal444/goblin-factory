﻿using GoblinFactory.Contract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoblinFactory.DataAccess.Models
{
    public class ForumSection
    {
        public ForumSection()
        {
            DataHistory = new DataHistory();

            ForumSubSection = new HashSet<ForumSubSection>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }

        public DataHistory DataHistory { get; set; }
        
        public virtual ICollection<ForumSubSection> ForumSubSection { get; set; }
    }
}
