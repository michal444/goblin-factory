﻿using GoblinFactory.Contract.Common;
using System;

namespace GoblinFactory.DataAccess.Models
{
    public class News
    {
        public News()
        {
            DataHistory = new DataHistory();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }

        public DataHistory DataHistory { get; set; }
    }
}
