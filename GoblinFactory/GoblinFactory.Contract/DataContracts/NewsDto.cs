﻿using GoblinFactory.Contract.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoblinFactory.Contract.DataContracts
{
    public class NewsDto
    {
        public NewsDto()
        {
            DataHistory = new DataHistory();
        }

        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public DataHistory DataHistory { get; set; }
    }
}
