﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GoblinFactory.Contract.Common
{
    public class DataHistory
    {
        public DateTime CreationDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
    }
}
