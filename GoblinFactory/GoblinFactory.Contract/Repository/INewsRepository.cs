﻿using GoblinFactory.Contract.DataContracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoblinFactory.Contract.Repository
{
    public interface INewsRepository
    {
        List<NewsDto> GetAll();
    }
}
