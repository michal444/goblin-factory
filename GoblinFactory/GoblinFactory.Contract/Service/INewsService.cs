﻿using GoblinFactory.Contract.DataContracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace GoblinFactory.Contract.Service
{
    public interface INewsService
    {
        List<NewsDto> GetAll();
    }
}
